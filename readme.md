# Exception email sender for Lavarel by Dense

Simple package for sending exceptions to user defind email.

## Instalation

Run following composer command.

```
composer require dense/laravel-informer
```

Publish configuration files
```
php artisan vendor:publish
```

## Configuration

Add next lines to Exception Handler in app/Excetions/Handler.php

Trait use at the top of the file.

```php
use Dense\Informer\Mail\InformerTrait;
```
Then this at top of the  Handler class
```php
use InformerTrait;
```

And then add this lines to report method
```php
public function report(Throwable $exception)
{
    parent::report($exception);
  
    if ($this->shouldReport($exception)) {
        $this->sendException($exception);
    }
}
```
In .env file add   
```
INFORMER_EMAIL = "your_email@example.com"
INFORMER_SUBJECT = "Application Logging System"
```
Do the same everytime you want to log some exception in try-catch block. For example:

```php

use Dense\Informer\Mail\InformerTrait;
  
class ImportantController
{
    use InformerTrait;
    
    public function importantAction {
        try {
            $somethingToSave->save();
        } catch (\Exception $e){
            $this->sendException($e);
        }
    }
}
```

If you are running lumen you need to copy/paste original email config and informer config from vendor to lumen config directory.  
Also add following lines to bootstrap/app.php file.

```php
$app->configure('mail');
$app->configure('informer');
  
$app->register(\Illuminate\Mail\MailServiceProvider::class);
$app->register(\Dense\Informer\InformerServiceProvider::class);
```
