@component('mail::message')

<strong>Message</strong><br>
{{ $exception->getMessage() }}<br>

<strong>Type</strong><br>
{{ get_class($exception) }}<br>

<strong>File</strong><br>
{{ $exception->getFile() }}<br>

<strong>Line</strong><br>
{{ $exception->getLine() }}<br>

<strong>Server</strong><br>
{{ $_SERVER['SERVER_ADDR'] . ' (' . $_SERVER['SERVER_NAME'] . ')' }}<br>

<strong>Ip</strong><br>
{{ $_SERVER['REMOTE_ADDR'] }}<br>

<strong>Url</strong><br>
{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}<br>

<strong>Method</strong><br>
{{ $_SERVER['REQUEST_METHOD'] }}<br>

<strong>Request body</strong><br>
{{ file_get_contents('php://input') }}<br>

@endcomponent
