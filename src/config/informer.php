<?php

return [
    'recipients' => env('INFORMER_EMAIL'),
    'subject' => env('INFORMER_SUBJECT', env('APP_NAME') . ' ' . 'Logging System'),
];
