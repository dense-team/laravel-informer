<?php

namespace Dense\Informer;

use Illuminate\Support\ServiceProvider;

class InformerServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'informer');

        $this->mergeConfigFrom(__DIR__ . '/config/informer.php', 'informer');

        $this->publishes([
            __DIR__ . '/config/informer.php' => base_path('config/informer.php'),
            __DIR__ . '/views' => resource_path('views/informer'),
        ]);
    }

    /**
     * @return void
     */
    public function register()
    {
    }
}
