<?php

namespace Dense\Informer\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Config;

class ExceptionNotification extends Mailable
{
    /**
     * @var \Throwable
     */
    public $exception;

    /**
     * Create a new message instance.
     *
     * @param \Throwable $exception
     * @return void
     */
    public function __construct(\Throwable $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = Config::get('informer.subject');

        return $this
            ->subject($subject)
            ->markdown('informer::exception');
    }
}
