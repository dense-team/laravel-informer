<?php

namespace Dense\Informer\Mail;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

trait InformerTrait
{
    /**
     * @return array
     */
    protected function getRecipients()
    {
        $recipients = explode(',', Config::get('informer.recipients'));

        return $recipients;
    }

    /**
     * @param \Throwable $exception
     * @return void
     */
    public function sendException(\Throwable $exception)
    {
        $recipients = $this->getRecipients();

        if (!empty($recipients)) {
            try {
                Mail::to($recipients)
                    ->send(new ExceptionNotification($exception));
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * @param \Throwable $serviceException
     * @return void
     */
    public function sendGuzzleException(\Throwable $serviceException)
    {
        $message = (string)$serviceException->getResponse()->getBody();
        $exception = new \Exception($message);

        $this->sendException($exception);
    }

    /**
     * @param string $debug
     * @return void
     */
    public function sendDebug($debug)
    {
        $recipients = $this->getRecipients();

        if (!empty($recipients)) {
            try {
                Mail::to($recipients)
                    ->send(new DebugNotification($debug));
            } catch (\Exception $e) {
            }
        }
    }
}
