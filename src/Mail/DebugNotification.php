<?php

namespace Dense\Informer\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Config;

class DebugNotification extends Mailable
{
    /**
     * @var string
     */
    public $debug;

    /**
     * Create a new message instance.
     *
     * @param string $debug
     * @return void
     */
    public function __construct($debug)
    {
        $this->debug = $debug;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = Config::get('informer.subject');

        return $this
            ->subject($subject)
            ->markdown('informer::debug');
    }
}
